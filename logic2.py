import requests as rq
from typing import List
import random

DEBUG = False

class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        self.choices = [w for w in MMBot.words if is_unique(w)]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']

        for _ in range(6):
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            if won:
                break
            self.update(choice, feedback)
            if not self.choices:
                break
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
        
        if won:
            print("Secret is", choice, "found in", len(tries), "attempts")
        else:
            print("Failed to find the secret word in 6 attempts.")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, feedback: str):
        def matches_feedback(word: str, choice: str, feedback: str) -> bool:
            for i in range(len(feedback)):
                if feedback[i] == 'Y' and (choice[i] not in word or word[i] == choice[i]):
                    return False
                if feedback[i] == 'G' and word[i] != choice[i]:
                    return False
                
                if feedback[i] == 'R' and choice[i] in word:
                    return False
            return True
        
        self.choices = [w for w in self.choices if matches_feedback(w, choice, feedback)]


game = MMBot("Kaamya")
game.play()

