import requests as rq
import random

DEBUG = False

class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "mastermind", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)

        self.choices = [w for w in MMBot.words[:] if is_unique(w)]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            right = self.generate_right(choice, rj["secret"])
            status = "win" in rj["message"]
            return right, status

        attempts = 0  
        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice}:{right}']
        attempts += 1

        while not won and attempts < 6: #so that max attempts can be changed from infinite to 6
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
            attempts += 1

        if won:
            print("Secret is", choice, "found in", len(tries), "attempts")
        else:
            print("Failed to find the secret word in 6 attempts.")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, right: str): #here i have changed feedback to become str instead of int 
        self.choices = [w for w in self.choices if self.generate_right(choice, w) == right]

    def generate_right(self, guess: str, secret: str) -> str: # this is the main function that i wrote to give the string feedback in terms of G,R,Y etc
        feedback = []
        right = []
        for g, s in zip(guess, secret):
            if g == s:
                right.append('G')
            elif g in secret:
                right.append('Y')
            else:
                right.append('R')
        return ''.join(right)

game = MMBot("CodeShifu")
game.play()
